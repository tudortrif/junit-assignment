import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testAddition()
    {
        Calculator calculator = new Calculator();
        Double result = calculator.compute(4, 7, "+");
        Assert.assertEquals(11, result,0);
    }

    @Test
    public void testSubstraction()
    {
        Calculator calculator = new Calculator();
        Double result = calculator.compute(4,7,"-");
        Assert.assertEquals(-3, result,0);
    }

    @Test
    public void testMultiplication()
    {
        Calculator calculator = new Calculator();
        Double result = calculator.compute(4, 7,"*");
        Assert.assertEquals(28,result,0);
    }

    @Test
    public void testDivision()
    {
        Calculator calculator = new Calculator();
        Double result = calculator.compute(4,7,"/");
        Assert.assertEquals(0.5714, result, 0.0001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDivisionByZero()
    {
        Calculator calculator = new Calculator();
        Double result = calculator.compute(4, 0,"/");
    }

    @Test
    public void testSquareRoot()
    {
        Calculator calculator =  new Calculator();
        Double result = calculator.compute(9,2, "SQRT");
        Assert.assertEquals(3, result, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDivisionByZero1()
    {
        Calculator calculator = new Calculator();
        Double result = calculator.compute(6, 0,"g");
    }
}
